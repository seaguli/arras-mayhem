# Update
- Added Radar branch from Sniper
- Added Base with Sunchip drones
# Update
- Added Hivemind branch from Inceptioner 
- Recolored Nest Keeper
- Fixed additional server crash bugs
- Buffed Punt Gun and Shatterer
# Update
- Fixed a bug where attempting to activate God Mode or suiciding while already dead would crash the server
# Update
- Improved Sentry AI
- Added 2 new Sentries
# Update
- Added even more tanks
- Buffed Heavy-3 Turret's firerate
# Update
- Added many new tanks
# Update
- Map has been redesigned, did I mention the Bases have names too?!
# Update
- The client has been updated with a lot of new functions! Try clicking on the Arras logo to try them out!
# Update
- Added a ton of new tanks
# Update
- Added Exploder tank and branch
# Update
- Fixed up tank branching
- Added Auto-tank branches
# Update
- Added 4 new tanks

- Heavy Twin - Branches off Twin

- Kraken - Branches off Octo Tank and Cyclone

- Stomper - Branches off Hexa Tank

- Frigate - Branches off Overseer and Cruiser

- Added 2 new bosses to the Testbed branch
# Update
- Some tanks are now unlocked at tier 4 (Lvl60)
- Added a handful of Spawner-branch tanks
# Update
- Bases can no longer be pushed
- Bots are now Smasher-branch tanks to curb lag
- Testbed Token is now public! Visit my Discord server to claim!
# Update
- Added 3 New Tanks:

- Hornet - Branches off Gunner and Tri-Angle

- Typhoon - Branches off Cyclone

- Shadow Tank - Branches off Basic at Lvl30

- Added music function to client (Press on view options)
# Update
-Added 3 Tanks:

- Creator - Branches off Spawner and Compass

- Underlord - Branches off Underseer

- Minelayer - Branches off Mega Trapper and Builder

- Minion firerate nerfed 
# Update
- Destroyer Branch Nerfed
- Sidewinder Nerfed
# Update
- Hunter Branch Buffed
- Tri-Angle Branch Nerfed
- Sprayer Buffed
# Update
- Map size doubled 
- Bases can now be pushed using bullets or drones
- Added Seeker, which branches off Homing and Spawner



